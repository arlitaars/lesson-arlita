package com.example.arlita.testcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EquationButton();
    }

    private void EquationButton() {

        final TextView firstInput = findViewById(R.id.first_input);
        final TextView secondInput = findViewById(R.id.second_input);

        final TextView equation = findViewById(R.id.equation_symbol);

        equation.setText("");

        final TextView result = findViewById(R.id.result);

        Button button0 = findViewById(R.id.button_0);
        Button button1 = findViewById(R.id.button_1);
        Button button2 = findViewById(R.id.button_2);
        Button button3 = findViewById(R.id.button_3);
        Button button4 = findViewById(R.id.button_4);
        Button button5 = findViewById(R.id.button_5);
        Button button6 = findViewById(R.id.button_6);
        Button button7 = findViewById(R.id.button_7);
        Button button8 = findViewById(R.id.button_8);
        Button button9 = findViewById(R.id.button_9);
        Button buttonDec = findViewById(R.id.button_dec);

        Button buttonEq = findViewById(R.id.button_eq);
        Button buttonAdd = findViewById(R.id.button_plus);
        Button buttonMin = findViewById(R.id.button_min);
        Button buttonMul = findViewById(R.id.button_mult);
        Button buttonDiv = findViewById(R.id.button_div);
        Button buttonCl = findViewById(R.id.button_c);
        Button buttonDel = findViewById(R.id.button_del);

        buttonEq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eq = equation.getText() + "";
                calculation(eq);
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "0";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "0";
                    secondInput.setText(tempInput);
                }

            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "1";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "1";
                    secondInput.setText(tempInput);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "2";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "2";
                    secondInput.setText(tempInput);
                }
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if(equat.equals("")) {
                    tempInput = firstInput.getText() + "3";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "3";
                    secondInput.setText(tempInput);
                }
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "4";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "4";
                    secondInput.setText(tempInput);
                }
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "5";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "5";
                    secondInput.setText(tempInput);
                }
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "6";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "6";
                    secondInput.setText(tempInput);
                }
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "7";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "7";
                    secondInput.setText(tempInput);
                }
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "8";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "8";
                    secondInput.setText(tempInput);
                }
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "9";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + "9";
                    secondInput.setText(tempInput);
                }
            }
        });

        buttonDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String equat = equation.getText() + "";

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + ".";
                    firstInput.setText(tempInput);
                } else {
                    tempInput = secondInput.getText() + ".";
                    secondInput.setText(tempInput);
                }
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equation.setText("+");
            }
        });

        buttonMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equation.setText("-");
            }
        });

        buttonMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equation.setText("*");
            }
        });

        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equation.setText("/");
            }
        });

        buttonCl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstInput.setText("");
                secondInput.setText("");
                equation.setText("");
                result.setText("");
            }
        });

        buttonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInput;
                String charSymbol;
                String equat = equation.getText() + "";

                Integer delete;

                if (equat.equals("")) {
                    tempInput = firstInput.getText() + "";
                    if (tempInput.equals("")) {
                        Toast.makeText(getApplicationContext(), "Tidak ada nilai", Toast.LENGTH_SHORT).show();
                    } else {
                        delete = tempInput.length();
                        tempInput = tempInput.substring(0, delete-1);
                        firstInput.setText(tempInput);
                    }
                } else {
                    tempInput = secondInput.getText() + "";
                    charSymbol = equation.getText() + "";
                    if (tempInput.equals("")) {
                        equation.setText("");
                        tempInput = firstInput.getText() + "";
                        if (tempInput.equals("")) {
                            Toast.makeText(getApplicationContext(), "Tidak ada nilai", Toast.LENGTH_SHORT).show();
                        } else {
                            delete = tempInput.length();
                            tempInput = tempInput.substring(0, delete-1);
                            firstInput.setText(tempInput);
                        }
                    } else {
                        delete = tempInput.length();
                        tempInput = tempInput.substring(0, delete-1);
                        secondInput.setText(tempInput);
                    }
                }
            }
        });
    }

    private void calculation(String equ) {

        TextView firstInput = findViewById(R.id.first_input);
        TextView secondInput = findViewById(R.id.second_input);
        TextView resultText = findViewById(R.id.result);

        String firstInputString = firstInput.getText() + "";
        String secondInputString = secondInput.getText() + "";

        Double firstInputValue = Double.parseDouble(firstInputString);
        Double secondInputValue = Double.parseDouble(secondInputString);

        String result = "";

        switch (equ) {
            case "+":
                result = firstInputValue + secondInputValue + "";
                resultText.setText(result);
                break;
            case "-":
                result = firstInputValue - secondInputValue + "";
                resultText.setText(result);
                break;
            case "*":
                result = firstInputValue * secondInputValue + "";
                resultText.setText(result);
                break;
            case "/":
                result = firstInputValue / secondInputValue + "";
                resultText.setText(result);
                break;
        }

    }
}
